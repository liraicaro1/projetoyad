#!/bin/bash




FORM=$(	#Cria variável FORM que recebe a saída do yad abaixo, especificamente do comando --field.

	# comando yad -> cria interface gráfica (janela)
	#  --center -> alinha ao centro
	#  --title -> adiciona um título a interface gráfica (janela)
       	#  --width -> configura largura da janela
	# --height -> configura altura da janela
	# --text-align -> permite alinhar a direita, esquerda, centro
	# --text -> permite inserir um texto na interface gráfica
	# --form -> cria um formulário
	# --field -> adiciona um campo no formulário de determinado tipo
	# MFL -> Tipo usado no --field para permitir selecionar vários arquivos	
yad --center								\
	--title="iHATEPDF"				\
	--width=250							\
	--height=250							\
	--window-icon=img/imagem.ico					\
	--image=img/imagem.ico						\
	--form								\
	--field="Escolha o(s) arquivo(s):":MFL  "$HOME/Documentos" 	\
)

function OPCAO {
OPC=$(
yad --center								\
	--title="iHATEPDF"				\
	--width=300							\
	--height=250							\
	--window-icon=img/imagem.ico					\
	--text-align=center						\
	--text="Exemplo de texto"					\
	--form								\
	--field="Escolha a opção que deseja executar":CB Juntar!Dividir!Excluir!Preview  	\
)
}
EXE=FALSE
if [ "$FORM" ==  "$HOME/Documentos|" ];
then
	yad --center	--no-buttons --posx=1				\
	--title="ERRO 001"						\
	--text-align=center						\
	--text="Nenhum arquivo selecionado. \n
		Por favor, selecione novamente os arquivos. \n
		A janela irá fechar automaticamente em 5 segundos."	\
	--window-icon=img/imagem.ico					\
	--timeout=5 --timeout-indicator=bottom				\
	echo | ./ihatepdf.sh
elif [ "$FORM" == "" ]
then
	exit
else
	EXE=TRUE
	CONT=$(echo $FORM | grep -o ".pdf" | wc -l)
	echo "$CONT Arquivos selecionados"
	i=0
	while [ $i -lt $CONT ];
	do	
		if [ $[$i+1] -eq $CONT ];
			then	
				echo "$FORM" | cut -d "|" -f 1 | cut -d "!" -f $CONT >> /tmp/caminho.txt
		else	
			echo "$FORM" | cut -d "!" -f $[$i+1] >> /tmp/caminho.txt
		fi
		i=$[$i+1]
	done
fi

if [ $EXE == TRUE ];
then
	OPCAO
fi
########################### EXCLUIR

function EXCLUIR {
PAG=$( yad --center \
	--title="iHATEPDF"				\
	--window-icon=img/imagem.ico					\
	--text-align=center	\
	--text="Escolha a página ou intervalo para permanecer no arquivo. \n 
	Se for apenas uma página digite o número da página. \n
       	Se for um intervalo digite ex: 1-3 6-10 para excluir 4 e 5." \
	--entry=	\
)
x=1
while [ $x -lt $[$CONT+1] ];
do
	NOME=$(sed -n "$x p" /tmp/caminho.txt | cut -d "/" -f 5 | sed -e 's/.pdf//')
	sed -n "$x p" /tmp/caminho.txt | sed -e 's/^/pdftk A="/' -e 's/$/"/' -e "s/$/ cat A$PAG/" -e 's/$/ output "/' -e "s/$/$NOME pagina $PAG.pdf/" -e 's/$/"/'  >> /tmp/arq.sh
x=$[$x+1]; 
done

chmod +x /tmp/arq.sh
/tmp/arq.sh
rm /tmp/arq.sh
rm /tmp/caminho.txt
}
################ JUNTAR
function JUNTAR {
	cat /tmp/caminho.txt | sed -e 's/^/"/g' -e 's/$/"/g' | tr "\n" " " | sed -e 's/^/pdftk /' -e 's/$/ cat output documento-mesclado.pdf/' >> /tmp/arq.sh

cat /tmp/caminho.txt
chmod +x /tmp/arq.sh
/tmp/arq.sh
rm /tmp/arq.sh
rm /tmp/caminho.txt
}

########################## PREVIEW
function PREVIEW {

x=1
while [ $x -lt $[$CONT+1] ];
do
	NOME=$(sed -n "$x p" /tmp/caminho.txt | cut -d "/" -f 5 | sed -e 's/.pdf//')

	sed -n "$x p" /tmp/caminho.txt | sed -e 's/^/pdftk A="/' -e 's/$/"/' -e "s/$/ cat 1/" -e 's/$/ output "/' -e "s/$/$NOME preview.pdf/" -e 's/$/"/'  >> /tmp/arq.sh
x=$[$x+1]; 
done

chmod +x /tmp/arq.sh
/tmp/arq.sh
rm /tmp/arq.sh
rm /tmp/caminho.txt

}

############################DIVIDIR

function DIVIDIR {
INTERVALO=$( yad --center \
	--title="iHATEPDF"				\
	--window-icon=img/imagem.ico					\
	--text-align=center	\
	--text="Escolha o intervalo para dividir o arquivo em duas partes. \n 
	 	O intervalo deve ser dividido por uma / exemplo: 1-5/6-10." \
	--entry=	\
)

INT1=$(echo $INTERVALO | cut -d "/" -f 1)
INT2=$(echo $INTERVALO | cut -d "/" -f 2)

x=1
while [ $x -lt $[$CONT+1] ];
do
	NOME=$(sed -n "$x p" /tmp/caminho.txt | cut -d "/" -f 5 | sed -e 's/.pdf//')
       	echo $NOME	
	sed -n "$x p" /tmp/caminho.txt | sed -e 's/^/pdftk A="/' -e 's/$/"/' -e "s/$/ cat A$INT1/" -e 's/$/ output "/' -e "s/$/$NOME pagina $INT1.pdf/" -e 's/$/"/'  >> /tmp/arq1.sh
	sed -n "$x p" /tmp/caminho.txt | sed -e 's/^/pdftk A="/' -e 's/$/"/' -e "s/$/ cat A$INT2/" -e 's/$/ output "/' -e "s/$/$NOME pagina $INT2.pdf/" -e 's/$/"/'  >> /tmp/arq2.sh
x=$[$x+1]; 
done

chmod +x /tmp/arq1.sh
chmod +x /tmp/arq2.sh
/tmp/arq1.sh
/tmp/arq2.sh
rm /tmp/arq1.sh
rm /tmp/arq2.sh
rm /tmp/caminho.txt
}

case $OPC in
	"Excluir|")
		EXCLUIR;;
	"Juntar|")
		JUNTAR;;
	"Preview|")
		PREVIEW;;
	"Dividir|")
		DIVIDIR;;
esac
